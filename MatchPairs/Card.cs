﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace MatchPairs
{
    class Card
    {
        // ---------------------------
        // Type Definitions
        // ---------------------------
        public enum Symbol
        // A special type of integer with only specific values allowed, that have names
        {
            CROCODILE,  // = 0
            HORSE,      // = 1
            ELEPHANT,   // = 2
            ZEBRA,        // = 3
            DOG,        // = 4

            // --

            NUM         // = 3
        }

        // ---------------------------
        // Data
        // ---------------------------
        Symbol ourSymbol;
        private Texture2D cardSprite;
        private Texture2D symbolSprite;
        Vector2 position = Vector2.Zero;
        bool flipped = false;
        MouseState previousMouseState;
        bool visible = true;


        // ---------------------------
        // Behaviour
        // ---------------------------
        public Card(Symbol newSymbol)
        {
            // Constructor. Called when the object is created.
            // No return type (special)
            // Helps decide how the object will be set up
            // Can have arguments
            ourSymbol = newSymbol;
        }
        // ---------------------------
        public void LoadContent(ContentManager content)
        {
            cardSprite = content.Load<Texture2D>("graphics/card");

            switch (ourSymbol)
            {
                case Symbol.CROCODILE:
                    symbolSprite = content.Load<Texture2D>("graphics/crocodile");
                    break;
                case Symbol.HORSE:
                    symbolSprite = content.Load<Texture2D>("graphics/horse");
                    break;
                case Symbol.ELEPHANT:
                    symbolSprite = content.Load<Texture2D>("graphics/elephant");
                    break;
                case Symbol.ZEBRA:
                    symbolSprite = content.Load<Texture2D>("graphics/zebra");
                    break;
                case Symbol.DOG:
                    symbolSprite = content.Load<Texture2D>("graphics/dog");
                    break;
                default:
                    // This should never happen
                    break;
            }
        }
        // ---------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            if (!visible)
                return;

            spriteBatch.Draw(cardSprite, position, Color.White);

            if (flipped)
            {
                // Work out position of symbol on card
                Rectangle symbolRect = new Rectangle();
                symbolRect.Width = symbolSprite.Width / 2;
                symbolRect.Height = symbolSprite.Height / 2;
                symbolRect.X = (int)position.X + cardSprite.Width / 2 - symbolRect.Width / 2;
                symbolRect.Y = (int)position.Y + cardSprite.Height / 2 - symbolRect.Height / 2;

                spriteBatch.Draw(symbolSprite, symbolRect, Color.White);
            }
        }
        // ---------------------------
        public void Input()
        {
            if (!visible)
                return;

            // Get the current status of the mouse
            MouseState currentMouseState = Mouse.GetState();

            // Get the bounding box for the object
            Rectangle bounds = new Rectangle(
                (int)position.X,
                (int)position.Y,
                cardSprite.Width,
                cardSprite.Height);

            // Check if we have the left button clicked over the object
            if (currentMouseState.LeftButton == ButtonState.Pressed
                && previousMouseState.LeftButton != ButtonState.Pressed
                && bounds.Contains(currentMouseState.X, currentMouseState.Y))
            {
                // Flip the object
                flipped = true;
            }

            // Store old state
            previousMouseState = currentMouseState;
        }
        // ---------------------------
        public void SetPosition(Vector2 newPosition)
        {
            position = newPosition;
        }
        // ---------------------------
        public bool GetFlipped()
        {
            return flipped;
        }
        // ---------------------------
        public Symbol GetSymbol()
        {
            return ourSymbol;
        }
        // ---------------------------
        public void Hide()
        {
            visible = false;
        }
        // ---------------------------
        public void Flip()
        {
            flipped = !flipped;
        }
        // ---------------------------


    }
}
