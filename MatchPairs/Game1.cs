﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Linq;

namespace MatchPairs
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        const int NUM_CARDS = 10;
        Card[] ourCards = new Card[NUM_CARDS];
        float timeSinceCardReveal = 0;
        const float CARD_REVEAL_DURATION = 2f;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            for (int i = 0; i < NUM_CARDS; ++i)
            {
                ourCards[i] = new Card((Card.Symbol) (i % (int)Card.Symbol.NUM));
                ourCards[i].LoadContent(Content);
            }

            // Randomly order our cards
            Random rnd = new Random();
            ourCards = ourCards.OrderBy(x => rnd.Next()).ToArray();

            // Position cards
            for (int i = 0; i < NUM_CARDS; ++i)
            {
                Vector2 position = new Vector2();
                position.X = 20 + (i % 5) * 150;
                position.Y = 20 + (i % 2) * 250;
                ourCards[i].SetPosition(position);
            }

            IsMouseVisible = true;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            // Check flip
            int numFlipped = 0;
            foreach (Card eachCard in ourCards)
            {
                if (eachCard.GetFlipped())
                    ++numFlipped;
            }

            if (numFlipped < 2) // not yet revealed
            {
                // Only process input on cards if there not already 2 flipped
                foreach (Card eachCard in ourCards)
                {
                    eachCard.Input();
                }
            }
            else { // cards are revealed

                // Add to timer
                timeSinceCardReveal += (float)gameTime.ElapsedGameTime.TotalSeconds;

                // Have we waited long enough?
                if (timeSinceCardReveal >= CARD_REVEAL_DURATION)
                {
                    // Find which two cards are flipped
                    Card firstCard = null;
                    Card secondCard = null;
                    foreach (Card eachCard in ourCards)
                    {
                        if (eachCard.GetFlipped())
                        {
                            if (firstCard == null)
                            {
                                firstCard = eachCard;
                            }
                            else
                            {
                                secondCard = eachCard;
                            }
                        }
                    }

                    // Flip them back no matter what (fixes logic with checking for flipped cards)
                    firstCard.Flip();
                    secondCard.Flip();

                    // If there was a match, also hide them
                    if (firstCard.GetSymbol() == secondCard.GetSymbol())
                    {
                        firstCard.Hide();
                        secondCard.Hide();
                    }

                    // reset timer
                    timeSinceCardReveal = 0;
                }
            }


            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here

            spriteBatch.Begin();
            
            foreach (Card eachCard in ourCards)
            {
                eachCard.Draw(spriteBatch);
            }

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
